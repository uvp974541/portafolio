<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\obras;
use App\Models\carpetas;
use Illuminate\Support\Facades\Storage;
class obrasController extends Controller
{
    
    public function index()
    {
        $obras = obras::select('obras.id','titulo','id_carpeta','carpeta','fecha','precio','descripcion','imagen')
        ->join('carpetas','carpetas.id','=','obras.id_carpeta')->get();
        $carpetas=carpetas::all();
        return view('obras', compact('obras','carpetas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'imagen' => 'required|image',
            'precio' => 'required|max_digits:10',
        ]);
        $imageName = time().'.'.$request->imagen->extension();
        $request->imagen->move(public_path('images'), $imageName); 
        $obra = new obras($request->input());
        $obra ->imagen = $imageName;
        $obra -> save();
        return redirect('obras')
        ->with('success','Obra guardada');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $obra= obras::find($id);
        $carpetas=carpetas::all();
        return view('editObra', compact('obra','carpetas'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $obra=obras::find($id);
        $request->validate([
            'imagen' => 'image',
            'precio' => 'max_digits:10',
        ]);
        
        if($request->hasFile('imagen')){
        Storage::delete(public_path('images').$obra->imagen);
        $imageName = time().'.'.$request->imagen->extension();
        $request->imagen->move(public_path('images'), $imageName); 
        $obra ->imagen = $imageName;
        }
        $obra->fill($request->input());
        $obra -> save();
        return redirect('obras')
        ->with('success','Obra actualizada');;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $obra = obras::find($id);
        $obra -> delete();
        return redirect('obras')
        ->with('success','Obra eliminada');
    }
}
