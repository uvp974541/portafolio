<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\carpetas;

class carpetasController extends Controller
{
    public function index()
    {
        $carpetas= carpetas::all();
        return view('carpetas', compact('carpetas'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'carpeta'=> 'required',
         ]);
         $carpeta = new carpetas($request->input());
         $carpeta -> save();
        return redirect('carpetas')
        ->with('success','Nueva carpeta guardada');
    }

    public function show(string $id)
    {
        $carpeta= carpetas::find($id);
        return view('editCarpetas', compact('carpeta'));
    }

    public function edit(string $id)
    {
        //
    }

    public function update(Request $request, string $id)
    {
        $carpeta=carpetas::find($id);
        $carpeta->fill($request->input())->saveOrFail();
        return redirect('carpetas')
        ->with('success','Carpeta actualizada');;
    }

    public function destroy(string $id)
    {
        $carpeta = carpetas::find($id);
        $carpeta -> delete();
        return redirect('carpetas')
        ->with('success','Carpeta eliminada');
    }
}
