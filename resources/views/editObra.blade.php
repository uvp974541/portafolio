@extends('plantilla')
@section('contenido')
<div class="row">
    <div class="col-md-4 offset-md-4">
        <div class="card">
            <div class="card-header bg-dark text-white">
                <h3 class="card-title">Edición</h3>
                <div class="card-body">
                    <form id="frmObras" method="POST" action="{{url("obras",[$obra])}}" enctype="multipart/form-data">
                        @method("PUT")
                        @csrf
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-heading"></i></span>
                            <input type="text" name="titulo" value="{{$obra->titulo}}" class="form-control" placeholder="Título">
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-folder-open"></i></span>
                            <select name="id_carpeta" class="form-select">
                                <option value="">Carpeta</option>
                                @foreach ($carpetas as $row)
                                @if($row->id==$obra->id_carpeta)
                                    <option selected value="{{$row->id}}">{{$row->carpeta}}</option>
                                @else
                                    <option value="{{$row->id}}">{{$row->carpeta}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-regular fa-calendar"></i></span>
                            <input type="date" name="fecha" value="{{$obra->fecha}}" class="form-control" placeholder="Fecha" >
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-align-left"></i></span>
                            <textarea name="descripcion" class="form-control" rows="3" placeholder="Descripción">{{$obra->descripcion}}</textarea>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-dollar-sign"></i></span>
                            <input type="number" name="precio" max="19000" value="{{$obra->precio}}" class="form-control" placeholder="Precio">
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-regular fa-image"></i></span>
                            <input type="file" accept="image/*" value="{{$obra->imagen}}" name="imagen" class="form-control">
                        </div>
                        <div class="mb-3 text-center">
                            <label>Imagen actual</label>
                            <button type="button" class="btn" onclick="modalPrev('{{$obra->imagen}}','{{$obra->titulo}}')"  data-bs-toggle="modal" data-bs-target="#ImagenPrev">
                            <img src="http://127.0.0.1:8000/images/{{$obra->imagen}}" width="200px">
                            </button>
                        </div>
                        <div class="d-grid col-6 mx-auto">
                            <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                        </div>              
                      </form>
                </div>
                
            </div>
        </div>
        
    </div>
</div>

<div class="modal fade" id="ImagenPrev" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="labelPrev">Mostrar Imagen</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body text-center" >
         <img id="imagenPrev" width="90%">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@vite('resources/js/listado.js')
@endsection