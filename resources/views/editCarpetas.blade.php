@extends('plantilla')
@section('contenido')
    <div class="row">
        <div class="col-md-4 offset-md-4">
            <div class="card">
                <div class="card-header bg-dark text-white">
                    <div class="card-body">
                        <form id="editCarpetas" method="POST" action="{{url("carpetas",[$carpeta])}}">
                            @method("PUT")
                            @csrf
                            <div class="input-group mb-3">
                                <span class="input-group-text"><i class="fa-solid fa-folder-open"></i></span>
                                <input type="text" name="carpeta" value="{{$carpeta->carpeta}}" class="form-control" placeholder="Carpeta" required>
                            </div>
                            <div class="d-grid col-6 mx-auto">
                                <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i>Guardar</button>
                            </div>              
                          </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection