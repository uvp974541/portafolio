@extends('plantilla')
@section('contenido')
@if($mensaje = Session::get('success'))
<div class="row divok">
    <div class="col-md-6 offset-md-3">
        <div class="alert alert-success">
           <i class="fa-solid fa-check"></i> {{$mensaje}}
        </div>
    </div>
</div>
@endif
<div class="row mt-3">
    <div class="col-md-4 offset-md-4">
        <div class="d-grid mx-auto">
            <button class="btn btn-dark" data-bs-toggle="modal" data-bs-target="#modalCarpetas" >
                <i class="fa-solid fa-circle-plus"></i> Añadir
            </button>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12 col-lg-8 offset-0 offset-lg-2">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th></th>   
                    </tr>
                </thead>
                <tbody class="table-group-divider">
                    @foreach ($carpetas as $i => $row)
                    <tr>
                        <td>{{ ($i+1) }}</td>
                        <td>{{$row->carpeta}}</td>
                        <td>
                            <a href="{{url("carpetas",[$row]) }}" class="btn btn-success" >
                                <i class="fa-solid fa-edit" aria-hidden="true"></i>
                            </a> 
                            <form method="POST" action="{{url('carpetas',[$row]) }}">
                                @method("DELETE")
                                @csrf
                                <button class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modalCarpetas" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel">Añadir carpeta</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form id="frmCarpetas" method="POST" action="{{url("carpetas")}}">
            @csrf
            <div class="input-group mb-3">
                <span class="input-group-text"><i class="fa-solid fa-folder-open"></i></span>
                <input type="text" name="carpeta" class="form-control" placeholder="Carpeta" required>
            </div>
            <div class="d-grid col-6 mx-auto">
                <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
            </div>              
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  @vite('resources/js/listado.js')
@endsection
