@extends('plantilla')
@section('contenido')
@if($mensaje = Session::get('success'))
<div class="row divok">
    <div class="col-md-6 offset-md-3">
        <div class="alert alert-success">
           <i class="fa-solid fa-check"></i> {{$mensaje}}
        </div>
    </div>
</div>
@endif
<div class="row mt-3">
    <div class="col-md-4 offset-md-4">
        <div class="d-grid mx-auto">
            <button class="btn btn-dark"  data-bs-toggle="modal" data-bs-target="#modalObras">
                <i class="fa-solid fa-circle-plus"></i> Añadir
            </button>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-10 offset-1">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Título</th>
                        <th>Carpeta</th>
                        <th>Fecha</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Imagen</th>
                        <th></th>   
                    </tr>
                </thead>
                <tbody class="table-group-divider">
                    @foreach ($obras as $i => $row)
                    <tr>
                        <td>{{ ($i+1) }}</td>
                        <td>{{$row->titulo}}</td> 
                        <td>{{$row->carpeta}}</td>   
                        <td>{{$row->fecha}}</td>
                        <td>{{$row->descripcion}}</td>
                        <td>{{$row->precio}}</td>
                        <td> <button  class="btn" onclick="modalImg('{{$row->imagen}}','{{$row->titulo}}')"  data-bs-toggle="modal" data-bs-target="#modalImagen">
                                <img src="images/{{$row->imagen}}" width="100px">
                            </button>
                        </td>
                        <td>
                            <a href="{{url("obras",[$row]) }}" class="btn btn-success" >
                                <i class="fa-solid fa-edit" aria-hidden="true"></i>
                            </a> 
                            <form method="POST" action="{{url('obras',[$row]) }}">
                                @method("DELETE")
                                @csrf
                                <button class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modalObras" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel">Agregar obra</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        @if ($errors->any())
<div class="row">
  <div class="col-md-6 offset-md-3">
    <div class="alert alert-danger">
      <ul>
      @foreach ($errors->all() as $e)
        <li>{{$e}}</li>  
      @endforeach
      </ul>
    </div>
  </div>
</div>
@endif
        <div class="modal-body">
          <form id="frmObras" method="POST" action="{{url("obras")}}" enctype="multipart/form-data" >
            @csrf
            <div class="input-group mb-3">
                <span class="input-group-text"><i class="fa-solid fa-heading"></i></span>
                <input type="text" name="titulo" class="form-control" placeholder="Título" required>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text"><i class="fa-solid fa-folder-open"></i></span>
                <select name="id_carpeta" class="form-select">
                    <option value="">Carpeta</option>
                    @foreach ($carpetas as $row)
                    <option value="{{$row->id}}">{{$row->carpeta}}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text"><i class="fa-regular fa-calendar"></i></span>
                <input type="date" name="fecha" class="form-control" placeholder="Fecha" required>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text"><i class="fa-solid fa-align-left"></i></span>
                <textarea name="descripcion" class="form-control"  rows="3"></textarea>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text"><i class="fa-solid fa-dollar-sign"></i></span>
                <input type="number" name="precio" max="19000" class="form-control" placeholder="Precio" required>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text"><i class="fa-regular fa-image"></i></span>
                <input type="file" accept="image/*" name="imagen" class="form-control" placeholder="Inserte Imagen" required>
            </div>
            <div class="d-grid col-6 mx-auto">
                <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
            </div>              
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>  

  <div class="modal fade" id="modalImagen" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="imgLabel">Mostrar Imagen</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body text-center" >
         <img id="imagenDisplay" width="90%">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@vite('resources/js/listado.js')
@vite('resources/css/estilos.css')
@endsection